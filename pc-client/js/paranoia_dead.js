//const SERVER = "45.33.48.119:9999";
const SERVER = "45.33.48.119:5555";
email = "";
keys = [];
keyfiles = [];

(function (){
	
	httpGetAsync = function(theUrl, callback){
		var xmlHttp = new XMLHttpRequest({mozSystem: true});
		xmlHttp.onreadystatechange = function() { 
			if (xmlHttp.readyState == 4 && xmlHttp.status == 200)
				callback(xmlHttp.responseText);
		}
		xmlHttp.open("GET", theUrl, true); // true for asynchronous 
		xmlHttp.send(null);
		console.log("sending request:", theUrl);
	}

	warning = function(s){
		$(".warningbox").text(s);
	}
	isempty = function(s){
		return s == null || s == "";	
	}

	
	getEmail = function(){
		email = Cookies.get('email');
		$("#paramEmail").text(email);
		return email;
	}
	serverReadKeys = function(){
		email = getEmail();
		if( !isempty(email) ){
			httpGetAsync("http://" + SERVER + "/" + email, serverOnReadKeys);
		}
		warning("*email is not set");
	}
	serverOnReadKeys = function(keylist){
		console.log("we got the keys! ", keylist);
		$("#keybox").val(keylist);
		keyfiles = keylist.split('\n');
		for(i=0; i<keyfiles.length; i++){
			
		}
	}
	buttonRefresh = function(){
		serverReadKeys();
	}
	
	setEmail = function(s){
		email = s;
		Cookies.set('email', email, 365);
		console.log("setting email:", email);
		$("#paramEmail").text(email);
		serverReadKeys();
	}
	
}.call(this));

$(document).ready(function(){
	
	$(".header").load("header.html");
	
	$("#buttonRefresh").click(function(){
		serverReadKeys();
	});
	
	
	$("#submitEmail").click(function(){
		setEmail($("#textEmail").val());
	});
	$("#textEmail").keypress(function(e){
		if(e.keyCode == 13)
			setEmail($("#textEmail").val());
	});
	email = getEmail();
});
// Cookies.set('email', 'stephandavid3@hotmail.com');
// load
// http://45.33.48.119:9999/email@example.com

