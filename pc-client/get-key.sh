id=$(gpg --list-keys $1 | grep -m 1 "uid" | grep -o "\].*" | tr -d "]>\n" | sed "s/ </\n/" | sed 's/^ //')
key=$(gpg -a --export $1)

printf "$id"'\n'"$key" > $2
