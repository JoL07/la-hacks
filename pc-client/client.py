#!/usr/bin/python2

#import gtk
#import webkit
import pyqrcode
import subprocess
import os
import BaseHTTPServer
import requests
import thread
from time import sleep

period = 5
workingDir = os.path.dirname(os.path.realpath(__file__))

subprocess.call(["bash", "get-ultimate-user.sh", "uu"])
emailFile = open("uu", "r")
myEmail = emailFile.read()
print(myEmail)
emailFile.close()
#myEmail = "marekp375@gmail.com"
server = "http://45.33.48.119:9999/"

def GenerateQR(email):
    #r = subprocess.call(["gpg", "--output", "tmp.gpg","-a", "--export", email])
    r = subprocess.call(["bash", "get-key.sh", email, "tmp.gpg"])
    gpgFile = open("tmp.gpg", "rb")
    gpgData = gpgFile.read()
    qr = pyqrcode.create(gpgData, error='L')
    qr.svg("gpg.svg", scale=4)
    #subprocess.call(["convert", "gpg.svg", "gpg.png"])
    subprocess.call(["rm", "tmp.gpg"])

def getKeys():
    r = requests.get(server + myEmail)
    files = r.text.split("\n")
    i = 0
    for f in files:
        print(f)
        rr = requests.get(server + myEmail + "/" + f)
        
        print(server+myEmail+"/"+f)
        #self.send_response(200, "OK")
        ff = open(f, "wb")
        ff.write(rr.content)
        ff.close()
        if 0 == subprocess.call(["gpg", "--import", f]):
            i = i+1
        #subprocess.call(["rm", f])
    gk = open("get-keys.html", "w")
    gk.write(str(i))
    gk.close()

class MyHandler(BaseHTTPServer.BaseHTTPRequestHandler):
    def __init__(self, req, cl_addr, serv):
        BaseHTTPServer.BaseHTTPRequestHandler.__init__(self, req, cl_addr, serv)

    def do_GET(self):
        if (self.path == "/show-qr.html"):
            GenerateQR(myEmail)
        elif (self.path == "/get-keys.html"):
            getKeys()
        elif (self.path == "/list-keys.html"):
            subprocess.call(["bash", "list-keys.sh", "list-keys.html"])

        path = workingDir + self.path
        if (not os.path.isfile(path)):
            self.send_response(404, "Not Found")
            self.send_header("Content-Length", 0)
            self.end_headers()
            
        else:
            respFile = open(path, "rb")
            contents = respFile.read() 
            self.send_response(200, "OK")
            self.send_header("Content-Length", len(contents))
            if (path[-4:] == ".svg"):
                self.send_header("Content-Type", "image/svg+xml")
            self.end_headers()
            self.wfile.write(contents)

def periodicGet():
    if(period < 1):
        return
    while 1:
        try:
            getKeys()
        except requests.exceptions.RequestException as e:
            print(e)
        sleep(period)
        

thread.start_new_thread(periodicGet, ())
            
httpd = BaseHTTPServer.HTTPServer(('', 5000), MyHandler)
httpd.serve_forever() 

#view = webkit.WebView()
#sw = gtk.ScrolledWindow()
#sw.add(view)
#win = gtk.Window(gtk.WINDOW_TOPLEVEL)
#win.add(sw)
#win.show_all()
#view.open(workingDir + "/show-qr.html")
#gtk.main()

