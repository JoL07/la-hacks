#!/usr/bin/python2
import BaseHTTPServer
import os
import subprocess

workingDir = os.path.dirname(os.path.realpath(__file__))

class MyHandler(BaseHTTPServer.BaseHTTPRequestHandler):
    def __init__(self, req, cl_addr, serv):
        BaseHTTPServer.BaseHTTPRequestHandler.__init__(self, req, cl_addr, serv)

    def do_GET(self):
        slash = self.path.find('/', 1)
        if (slash == -1):
            if (os.path.exists(workingDir + self.path)):
                resp = "\n".join(os.listdir(workingDir + self.path))
            else:
                resp = ""
        else:
            respFile = open(workingDir + self.path)
            resp = respFile.read()
            #subprocess.call(["rm", workingDir + self.path])

        self.send_response(200, "OK")
        self.send_header("Content-Length", len(resp))
        self.send_header("Access-Control-Allow-Origin", "*")
        self.end_headers()
        self.wfile.write(resp)

    def do_PUT(self):
        slash = self.path.find('/', 1)
        user = self.path[1:slash]
        filename = self.path[slash+1:]
        dirname = self.path[1:slash]
        if (not os.path.exists(workingDir + "/" + dirname)):
            subprocess.call(["mkdir", dirname])

        f = open(workingDir + self.path, "wb")
        data = self.rfile.read(int(self.headers.get("Content-Length")))
        f.write(data)

        self.send_response(200, "OK")
        self.send_header("Content-Length", 0)
        self.end_headers()

httpd = BaseHTTPServer.HTTPServer(('', 9999), MyHandler)
httpd.serve_forever()

