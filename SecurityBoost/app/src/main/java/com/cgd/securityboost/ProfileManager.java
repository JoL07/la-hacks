package com.cgd.securityboost;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.text.InputType;
import android.widget.EditText;

/**
 * Created by john_ on 4/30/2016.
 */
public class ProfileManager {
    private static ProfileManager mInstance = new ProfileManager();
    private ProfileManager() {
        mEmail = null;
        mActivity = null;
    }
    public static ProfileManager getInstance() { return mInstance; }

    private String mEmail;
    private final String STORAGE_FILE = "__user__email";
    private Activity mActivity;

    public void init(Activity act) {
        mActivity = act;
        mEmail = null;
        byte[] temp = FileBrowser.readFile(mActivity, STORAGE_FILE);
        if (temp == null) {
            openDialogForEmail();
        }
        else
            mEmail = new String(temp);
    }

    public void openDialogForEmail() {
        mActivity.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                AlertDialog.Builder builder = new AlertDialog.Builder(mActivity);
                builder.setTitle("Enter Your Email");
                String curEmail = mEmail;
                if (curEmail == null)
                    curEmail = "None";
                builder.setMessage("This email is used to synchronize the keys you've received on your phone with your PC application.\nCurrent Email: " + curEmail);

                // Set up the input
                final EditText input = new EditText(mActivity);
                input.setInputType(InputType.TYPE_CLASS_TEXT);
                builder.setView(input);

                // Set up the buttons
                builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        mEmail = input.getText().toString().trim();
                        if (mEmail.isEmpty())
                            mEmail = null;
                        else
                            FileBrowser.writeFile(mActivity, STORAGE_FILE, mEmail);
                    }
                });
                builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        //do nothing
                    }
                });

                builder.show();
            }
        });
    }

    public String getEmail() {return mEmail;}
}
