package com.cgd.securityboost;

import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Base64;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.nfc.NdefMessage;
import android.nfc.NdefRecord;
import android.nfc.NfcAdapter;
import android.nfc.NfcAdapter.CreateNdefMessageCallback;
import android.nfc.NfcEvent;
import android.os.Parcelable;
import java.util.Arrays;

import java.net.HttpURLConnection;
import java.nio.charset.Charset;
import android.app.Activity;
import android.content.Intent;
import android.widget.TextView;
import android.widget.Toast;

import java.net.URL;
import java.io.*;
//import java.io.InputStream;

import org.openintents.openpgp.IOpenPgpService2;
import org.openintents.openpgp.OpenPgpDecryptionResult;
import org.openintents.openpgp.OpenPgpError;
import org.openintents.openpgp.OpenPgpSignatureResult;
import org.openintents.openpgp.util.OpenPgpApi;
import org.openintents.openpgp.util.OpenPgpServiceConnection;
import org.openintents.openpgp.util.OpenPgpUtils;
import org.w3c.dom.Text;

import com.google.zxing.integration.android.IntentIntegrator;
import com.google.zxing.integration.android.IntentResult;

public class MainActivity extends AppCompatActivity implements CreateNdefMessageCallback {

    NfcAdapter mNfcAdapter;
    OpenPgpServiceConnection mServiceConnection;
    byte[] mDataToSend;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        mDataToSend = null;

        findViewById(R.id.pgpexchange_btn).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(v.getContext(), PGPExchangeActivity.class);
                startActivity(intent);
            }
        });

        findViewById(R.id.pwboost_btn).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(v.getContext(), PasswordBoostActivity.class);
                startActivity(intent);
            }
        });

        findViewById(R.id.key_list_btn).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(v.getContext(), FileBrowser.class);
                intent.putExtra(getString(R.string.extensions), new String[]{PGPExchangeActivity.EXT});
                intent.putExtra(getString(R.string.ignore_other_ext), true);
                startActivityForResult(intent, 1);
            }
        });

        // initialize NFC
        mNfcAdapter = NfcAdapter.getDefaultAdapter(this);
        mNfcAdapter.setNdefPushMessageCallback(this, this);

        // initialize PGP
        mServiceConnection = new OpenPgpServiceConnection(this, "org.sufficientlysecure.keychain");
        mServiceConnection.bindToService();

        ProfileManager.getInstance().init(this);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater=getMenuInflater();
        inflater.inflate(R.menu.options, menu);
        return true;//return true so that the menu pop up is opened

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch(item.getItemId())
        {
            case R.id.change_email_opt:
                ProfileManager.getInstance().openDialogForEmail();
                break;
            case R.id.clear_opt:
                mDataToSend = null;
                ((TextView) findViewById(R.id.status_tv)).setText("");
                ((TextView) findViewById(R.id.nfc_tv)).setText("");
                break;
        }
        return true;
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == 1) {
            if (resultCode == RESULT_OK) {
                final String file = data.getExtras().getString(getString(R.string.file_path));
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        mDataToSend = FileBrowser.readFile(getApplicationContext(), file);
                        display(false, mDataToSend);
                    }
                });
            }
        }
    }

    public String byteToString(byte[] b) {
        String result = null;
        try {
            result = new String(b, "UTF-8");
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
            Toast.makeText(this, e.getMessage(), Toast.LENGTH_LONG).show();
        }
        return result;
    }

    protected void display(boolean rcvd, byte[] text) {
        String str = byteToString(text);
        if (rcvd) {
            ((TextView)findViewById(R.id.status_tv)).setText("NFC Results:");
        }
        else {
            ((TextView)findViewById(R.id.status_tv)).setText("NFC Ready!\nKey to Send:");
        }
        ((TextView)findViewById(R.id.nfc_tv)).setText(str);
    }

    @Override
    public NdefMessage createNdefMessage(NfcEvent event) {
        if (mDataToSend == null) {
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    Toast.makeText(getApplicationContext(), "No key selected to send", Toast.LENGTH_LONG).show();
                }
            });
            return null;
        }
        NdefMessage msg = new NdefMessage(
                new NdefRecord[] { NdefRecord.createMime(
                        "application/com.cgd.securityboost", mDataToSend)
                        /**
                         * The Android Application Record (AAR) is commented out. When a device
                         * receives a push with an AAR in it, the application specified in the AAR
                         * is guaranteed to run. The AAR overrides the tag dispatch system.
                         * You can add it back in to guarantee that this
                         * activity starts when receiving a beamed message. For now, this code
                         * uses the tag dispatch system.
                        */
                        ,NdefRecord.createApplicationRecord("com.cgd.securityboost")
                });
        return msg;
    }

    @Override
    public void onResume() {
        super.onResume();
        // Check to see that the Activity started due to an Android Beam
        if (NfcAdapter.ACTION_NDEF_DISCOVERED.equals(getIntent().getAction())) {
            new AsyncTask<Void, Void, Void>() {
                @Override
                protected Void doInBackground(Void... params) {
                    processIntent(getIntent());
                    return null;
                }
            }.execute();
        }
    }

    @Override
    public void onNewIntent(Intent intent) {
        // onResume gets called after this to handle the intent
        setIntent(intent);
    }

    /**
     * Parses the NDEF Message from the intent and prints to the TextView
     */
    void processIntent(Intent intent) {
        String email = ProfileManager.getInstance().getEmail();

        //textView = (TextView) findViewById(R.id.textView);
        Parcelable[] rawMsgs = intent.getParcelableArrayExtra(
                NfcAdapter.EXTRA_NDEF_MESSAGES);
        // only one message sent during the beam
        Parcelable p = rawMsgs[0];
        NdefMessage msg = (NdefMessage) rawMsgs[0];
        // record 0 contains the MIME type, record 1 is the AAR, if present
        //textView.setText(new String(msg.getRecords()[0].getPayload()));
        HttpURLConnection c = null;
        final byte[] nfcMsg = msg.getRecords()[0].getPayload();

        String fileName = null;
        String[] lines = byteToString(nfcMsg).split("\\r?\\n");
        if (lines.length > 0) {
            if (!lines[0].contains(PGPExchangeActivity.BEGIN_PGP_LINE)) {
                fileName = lines[0].trim().replace(' ', '-');
            }
        }
        final boolean invalidFormat = email == null || fileName == null;
        final String fileNameWithExt = (fileName == null ? "" : fileName) + "." + PGPExchangeActivity.EXT;
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                if (invalidFormat) {
                    Toast.makeText(getApplicationContext(), "Invalid format for transfer! Please put name and email on the first two lines.", Toast.LENGTH_LONG).show();
                }
                else {
                    FileBrowser.writeFile(getApplicationContext(), fileNameWithExt, byteToString(nfcMsg));
                    getIntent().setAction(null);
                    mDataToSend = null;
                    display(true, nfcMsg);
                }
            }
        });
        if (invalidFormat)
            return;
        try {
            URL url = new URL("http://45.33.48.119:9999/" + email + "/" + fileNameWithExt);
            Log.d("Upload", url.getPath());
            c = (HttpURLConnection) url.openConnection();
            c.setDoOutput(true);
            c.setRequestMethod("PUT");
            //c.setRequestProperty("Content-Length", Integer.toString(msg.getRecords()[0].getPayload().length));
            OutputStream out = new BufferedOutputStream(c.getOutputStream());
            /*byte[] rawKey = msg.getRecords()[0].getPayload();
            Intent data = new Intent();
            data.setAction(OpenPgpApi.ACTION_SIGN);
            data.putExtra(OpenPgpApi.EXTRA_USER_ID, "example@example.com");
            InputStream is = new ByteArrayInputStream(rawKey);
            ByteArrayOutputStream os = new ByteArrayOutputStream();
            OpenPgpApi api = new OpenPgpApi(this, mServiceConnection.getService());
            Intent result = api.executeApi(data, is, os);*/
            // assume success

            // assume that the server is not malicious for now
            out.write(nfcMsg);
            out.flush();
            out.close();
            c.connect();
            //out.write(os.toByteArray());
            //InputStream in = new BufferedInputStream(c.getInputStream());
            c.getInputStream();
            c.disconnect();
        }
        catch (Exception e) {
            e.printStackTrace();
            if (c != null)
                c.disconnect();
            // ?????
        }
    }

    public void onDestroy() {
        super.onDestroy();
        if (mServiceConnection != null) {
            mServiceConnection.unbindFromService();
        }
    }

}
