package com.cgd.securityboost;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.os.Environment;
import android.support.v7.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import java.io.BufferedOutputStream;
import java.io.DataInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

public class FileBrowser extends AppCompatActivity {
    private FileSystemAdapterView mAdapter = null;
    private ListView mListView = null;
    private File mCurParentDir = null;
    private ArrayList<String> mExtensions = null;
    private Boolean mIgnoreOtherExt = null;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.file_system_browser);

        setTitle("PGP Keys");

        Bundle args = getIntent().getExtras();
        String[] ext = null;
        if (args != null)
            ext = args.getStringArray(getString(R.string.extensions));
        if (ext != null) {
            mExtensions = new ArrayList<>();
            mExtensions.addAll(Arrays.asList(ext));
            mIgnoreOtherExt = args.getBoolean(getString(R.string.ignore_other_ext));
        }
        if (mIgnoreOtherExt == null)
            mIgnoreOtherExt = false;

        mListView = (ListView) findViewById(R.id.file_system_browser_layout);
        updateAdapter(getFilesDir());
        mListView.setAdapter(mAdapter);
        mListView.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
            @Override
            public boolean onItemLongClick(AdapterView<?> parent, View view, int position, long id) {
                final File itemToDelete = (File)(mListView.getItemAtPosition(position));
                new AlertDialog.Builder(view.getContext())
                        .setTitle("Delete Key")
                        .setMessage("Are you sure you want to delete this key?")
                        .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {
                                if (itemToDelete.delete()) {
                                    runOnUiThread(new Runnable() {
                                        @Override
                                        public void run() {
                                            updateAdapter(getFilesDir());
                                            Toast.makeText(getApplicationContext(), "Successfully deleted key!", Toast.LENGTH_SHORT).show();
                                        }
                                    });
                                }
                                else
                                    Toast.makeText(getApplicationContext(), "Failed to delete key!", Toast.LENGTH_SHORT).show();
                            }
                        })
                        .setNegativeButton(android.R.string.no, new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {
                                // do nothing
                            }
                        })
                        .setIcon(android.R.drawable.ic_dialog_alert)
                        .show();
                return true;
            }
        });
        mListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                File fileOrDir = mAdapter.getItem(position);
                if (position == 0 && mCurParentDir != null) { //selected parent directory
                    updateAdapter(mCurParentDir);
                }
                else if (fileOrDir.isDirectory()) { //if the selected item is a directory then open up new list of files and directories.
                    updateAdapter(fileOrDir);
                }
                else {
                    if (validExtension(getExtension(fileOrDir)))
                    {
                        Intent resultIntent = new Intent();
                        resultIntent.putExtra(getString(R.string.file_path), fileOrDir.getName());
                        setResult(AppCompatActivity.RESULT_OK, resultIntent);
                        finish();
                    }
                }
            }
        });
    }

    private void updateAdapter(File extDir) {
        ArrayList<File> fileList = new ArrayList<File>();
        mCurParentDir = null;
        File fls[] = extDir.listFiles();
        ArrayList<File> files = new ArrayList<>();
        if (fls != null)
            files.addAll(Arrays.asList(fls));
        Collections.sort(files);
        ArrayList<File> temp = new ArrayList<>();
        for (File f : files) {
            if (!f.isDirectory()) {
                if (!mIgnoreOtherExt || validExtension(getExtension(f)))
                    temp.add(f);
            }
        }
        fileList.addAll(temp);
        mAdapter = new FileSystemAdapterView(this, R.layout.file_item, fileList);
        mListView.setAdapter(mAdapter);
    }

    @Override
    public void onBackPressed() {
        Intent resultIntent = new Intent();
        setResult(AppCompatActivity.RESULT_CANCELED, resultIntent);
        finish();
        super.onBackPressed();
    }

    private boolean validExtension(String ext) {
        if (mExtensions == null)
            return true;
        return mExtensions.contains(ext.toLowerCase());
    }

    private String getExtension(File file) {
        int i = file.getName().lastIndexOf('.');
        if (i > 0)
            return file.getName().substring(i+1);
        return "";
    }

    private class FileSystemAdapterView extends ArrayAdapter<File> {
        Context context;

        public FileSystemAdapterView(Context context, int resourceId, List<File> items) {
            super(context, resourceId, items);
            this.context = context;
        }

        private class ViewHolder {
            ImageView iv;
            TextView tv;
        }

        public View getView(int position, View convertView, ViewGroup parent) {
            ViewHolder holder;
            File file = getItem(position);

            LayoutInflater mInflater = (LayoutInflater) context.getSystemService(AppCompatActivity.LAYOUT_INFLATER_SERVICE);
            if (convertView == null) {
                convertView = mInflater.inflate(R.layout.file_item, null);
                holder = new ViewHolder();
                holder.iv = (ImageView) convertView.findViewById(R.id.file_dir_icon);
                holder.tv = (TextView) convertView.findViewById(R.id.file_dir_name);
                convertView.setTag(holder);
            } else
                holder = (ViewHolder) convertView.getTag();

            String text = file.getName();
            if (file.isDirectory()) {
                holder.tv.setTextColor(Color.parseColor("#000000"));
                holder.iv.setImageDrawable(getDrawable(R.drawable.ic_menu_archive));
            }
            else {
                if (!validExtension(getExtension(file)))
                    holder.tv.setTextColor(Color.parseColor("#999999"));
                holder.iv.setImageDrawable(getDrawable(R.drawable.ic_menu_copy_holo_light));
            }
            holder.tv.setText(text);

            return convertView;
        }
    }

    public static byte[] readFile(Context context, String filePath) {
        byte[] fileData = null;
        byte[] realFileData = null;
        try {
            FileInputStream fis = context.openFileInput(filePath);
            DataInputStream dis = new DataInputStream(fis);
            fileData = new byte[4096];
            int n = 0;
            int count = 0;
            while ((count = dis.read(fileData, n, 1)) > 0) {
                n += count;
            }
            dis.close();
            realFileData = new byte[n];
            realFileData = Arrays.copyOf(fileData, n);

        } catch (IOException e) {
            e.printStackTrace();
        }
        return realFileData;
    }

    public static void writeFile(Context context, String filePath, String content) {
        try {
            FileOutputStream outputStream = context.openFileOutput(filePath, Context.MODE_PRIVATE);
            BufferedOutputStream out = new BufferedOutputStream(outputStream);
            out.write(content.getBytes());
            out.flush();
            outputStream.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}