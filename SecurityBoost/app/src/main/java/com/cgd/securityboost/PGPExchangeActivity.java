package com.cgd.securityboost;

import android.content.Context;
import android.content.Intent;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.zxing.integration.android.IntentIntegrator;
import com.google.zxing.integration.android.IntentResult;

import org.w3c.dom.Text;

import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.util.ArrayList;
import java.util.Arrays;

/**
 * Created by john_ on 4/30/2016.
 */
public class PGPExchangeActivity extends AppCompatActivity {
    public static final String PREFIX = "key";
    public static final String EXT = "pgp";
    public static final String BEGIN_PGP_LINE = "BEGIN PGP PUBLIC KEY BLOCK";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pgp_exchange);
        setTitle("Public Key QR Code Reader");
        scanQRCode();
    }

    public void scanQRCode() {
        IntentIntegrator integrator = new IntentIntegrator(this);
        integrator.initiateScan();
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent intent) {
        IntentResult scanResult = IntentIntegrator.parseActivityResult(requestCode, resultCode, intent);
        if (scanResult != null) {
            ((TextView) findViewById(R.id.pgp_tv)).setText("Failed to get PGP Key from QR Code!");
            try {
                String name = null;
                String content = scanResult.getContents();
                String[] lines = content.split("\\r?\\n");
                if (lines.length > 0) {
                    if (!lines[0].contains(BEGIN_PGP_LINE)) {
                        name = lines[0].trim();
                    }
                }
                if (name == null) {
                    int num = 0;
                    ArrayList<Integer> usedNumbers = new ArrayList<>();
                    String[] files = fileList();
                    for (int i = 0; i < files.length; i++) {
                        if (files[i].indexOf(PREFIX) == 0 && files[i].indexOf(EXT) == files[i].length() - EXT.length()) {
                            usedNumbers.add(Integer.valueOf(files[i].subSequence(PREFIX.length(), files[i].indexOf("." + EXT)).toString()));
                        }
                    }
                    for (int i = 0; i < 100; i++) {
                        if (usedNumbers.indexOf(i) < 0) {
                            num = i;
                            break;
                        }
                    }
                    name = PREFIX + Integer.toString(num);
                }

                FileBrowser.writeFile(this, name + "." + EXT, content);
                ((TextView) findViewById(R.id.pgp_tv)).setText(scanResult.toString());
            }
            catch (Exception e) {
                Log.e("Exception", "File write failed: " + e.toString());
            }
        }
    }

}
