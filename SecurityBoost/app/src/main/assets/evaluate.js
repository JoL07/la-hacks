function score(password) {
    var sUppers = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
    var sLowers = "abcdefghijklmnopqrstuvwxyz";
    var sLetters = sLowers + sUppers;
	var sNumerics = "01234567890";
	var sSymbols = ")!@#$%^&*() ";
    
    var minLength = 8;
    var numUpper = 0;
    var numLower = 0;
    var numNumeric = 0;
    var numSymbol = 0;
    
    var consecutive = 0;
    var lastWasUpper = false;
    var lastWasLower = false;
    var lastWasNumeric = false;
    var sequential = 0;
    var lastIndexLetter = -3;
    var lastIndexNumeric = -3;
    
    var length = password.length;
    var result = length * 4;
    for (var i = 0; i < length; i++) {
        var c = password[i];
        
        // Update requirements
        if (sUppers.indexOf(c) > -1)
            numUpper += 1;
        if (sLowers.indexOf(c) > -1)
            numLower += 1;
        if (sNumerics.indexOf(c) > -1)
            numNumeric += 1;
        if (sSymbols.indexOf(c) > -1)
            numSymbol += 1;
        
        // Symbols
        if (sSymbols.indexOf(c) > -1)
            result += 1;
        // Middle symbols/numbers
        if (i != 0 && i != length - 1)
            if (sNumerics.indexOf(c) > -1 || sSymbols.indexOf(c) > -1)
                result += 2;
        
        // Consecutive/sequential letters/numbers
        if (consecutive == 0)
            consecutive = 1;
        else if (lastWasUpper && sUppers.indexOf(c) > -1)
            consecutive += 1;
        else if (lastWasLower && sLowers.indexOf(c) > -1)
            consecutive += 1;
        else if (lastWasNumeric && sNumerics.indexOf(c) > -1)
            consecutive += 1;
        else {
            if (consecutive >= 3)
                result -= 2 * consecutive;
            consecutive = 0;
        }
        if (sequential == 0)
            sequential = 1;
        else if (sLetters.indexOf(c) % 26 == lastIndexLetter + 1)
            sequential += 1;
        else if (sNumerics.indexOf(c) == lastIndexNumeric + 1)
            sequential += 1;
        else {
            if (sequential >= 3)
                result -= 2 * sequential;
            sequential = 0;
        }
        if (sLetters.indexOf(c) > -1) {
            if (sUppers.indexOf(c) > -1)
                lastWasUpper = true;
            else
                lastWasLower = true;
            lastIndexLetter = sLetters.indexOf(c) % 26;
        }
        else {
            lastWasUpper = false;
            lastWasLower = false;
            lastIndexLetter = -3;
        }
        if (sNumerics.indexOf(c) > -1) {
            lastWasNumeric = true;
            lastIndexNumeric = sNumerics.indexOf(c);
        }
        else {
            lastWasNumeric = false;
            lastIndexNumeric = -3;
        }
        
        // Repeated characters
        for (var j = 0; j < length; j++)
            if (password[i] == password[j] && i != j)
                result -= length / Math.abs(j - i);
    }

    // Add from requirements
    if (length >= minLength)
        result += 2;
    if (numUpper)
        result += 2;
    if (numLower)
        result += 2;
    if (numNumeric)
        result += 2;
    if (numSymbol)
        result += 2;
    
    // Letters/numbers only
    if ((numUpper || numLower) && !numNumeric && !numSymbol)
        result -= length;
    else if (!numUpper && !numLower && numNumeric && !numSymbol)
        result -= length;
    else {
        result += 2 * (length - numUpper);
        result += 2 * (length - numLower);
        result += 4 * numNumeric;
    }
    
    // Deduct from consecutive/sequential
    if (consecutive >= 3)
        result -= 2 * consecutive;
    if (sequential >= 3)
        result -= 2 * sequential;
    
    return result;
}