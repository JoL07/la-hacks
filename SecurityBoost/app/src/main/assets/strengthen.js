function randomBool() {
    return Math.random() < 0.5;
}

function randomIndex(iterable) {
    return Math.floor(Math.random() * iterable.length);
}

var sUppers = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
var sLowers = "abcdefghijklmnopqrstuvwxyz";
var sNumerics = "01234567890";
var sSymbols = ")!@#$%^&*() ";

function toggleCases(word) {
    var modded = word;
    for (var i = 0; i < word.length; i++) {
        if (randomBool()) {
            if (randomBool()) {
                modded = word.substr(0, i) + word.substr(i, i+1).toUpperCase() + word.substr(i+1);
            }
            else {
                switch (word[i]) {
                    case 'O':
                    case 'o':
                        modded = word.substr(0, i) + "0" + word.substr(i+1);
                        word = modded;
                        break;
                    case 'L':
                    case 'l':
                        modded = word.substr(0, i) + "1" + word.substr(i+1);
                        word = modded;
                        break;
                    case 'R':
                    case 'r':
                        modded = word.substr(0, i) + "2" + word.substr(i+1);
                        word = modded;
                        break;
                    case 'E':
                    case 'e':
                        modded = word.substr(0, i) + "3" + word.substr(i+1);
                        word = modded;
                        break;
                    case 'A':
                    case 'a':
                    case 'H':
                    case 'h':
                        modded = word.substr(0, i) + "4" + word.substr(i+1);
                        word = modded;
                        break;
                    case 'S':
                    case 's':
                        modded = word.substr(0, i) + "5" + word.substr(i+1);
                        word = modded;
                        break;
                    case 'G':
                    case 'g':
                        modded = word.substr(0, i) + "6" + word.substr(i+1);
                        word = modded;
                        break;
                    case 'T':
                    case 't':
                        modded = word.substr(0, i) + "7" + word.substr(i+1);
                        word = modded;
                        break;
                    case 'B':
                    case 'b':
                        modded = word.substr(0, i) + "8" + word.substr(i+1);
                        word = modded;
                        break;
                    case 'Q':
                    case 'q':
                        modded = word.substr(0, i) + "9" + word.substr(i+1);
                        word = modded;
                        break;
                }
            }
        }
    }
    
    if (randomBool()) {
        var symbol = sSymbols[randomIndex(sSymbols)];
        modded = symbol + modded;
        word = modded;
    }
    if (randomBool()) {
        var symbol = sSymbols[randomIndex(sSymbols)];
        modded = modded + symbol;
        word = modded;
    }
    
    return modded;
}

function addWord(password) {
    var word;
    var wordValid = true;
    do {
        word = englishWords[randomIndex(englishWords)];
        for (var i = 0; i < word.length; i++)
            if (sUppers.indexOf(word[i]) < 0 && sLowers.indexOf(word[i]) < 0 && word[i] != "'") {
                wordValid = false;
                break;
            }
    } while (!wordValid);
    
    var modded = toggleCases(word);
    var split = randomIndex(password);
    return password.substr(0, split) + modded + password.substr(split);
}

function addNumber(password) {
    var number = sNumerics[randomIndex(sNumerics)] + sNumerics[randomIndex(sNumerics)];
    var split = randomIndex(password);
    return password.substr(0, split) + number + password.substr(split);
}

function addSymbol(password) {
    var sSymbols = ")!@#$%^&*() ";
    var symbol = sSymbols[randomIndex(sSymbols)];
    var split = randomIndex(password);
    return password.substr(0, split) + symbol + password.substr(split);
}